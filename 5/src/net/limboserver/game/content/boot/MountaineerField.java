// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.content.boot;

import net.limboserver.game.engine.control.Engine;
import javafx.application.Application;

public class MountaineerField {
	public static final String
		BOOT_SCRIPT = "/net/limboserver/game/content/map/MountaineerField.txt";
	public static void main(String args[]) {
		Application.launch(Engine.class, BOOT_SCRIPT);
	}
}
