// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.content.clr;

import net.limboserver.game.engine.helper.PaintSheme;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;

public class Gradients implements PaintSheme {
	public Paint getPaint (String paintname) {
		if (paintname.equals("OceanSunset")) {
			Stop[] stops = new Stop[] {
					new Stop(0.0, Color.INDIGO),
					new Stop(0.4, Color.CHOCOLATE),
					new Stop(0.6, Color.GOLD),
					new Stop(0.675, Color.YELLOW),
					new Stop(0.7, Color.SALMON),
					new Stop(0.8, Color.CRIMSON),
					new Stop(1.0, Color.PURPLE)
			};
			return new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
		}
		
		if (paintname.equals("SummerDay")) {
			Stop[] stops = new Stop[] {
					new Stop(0.0, Color.BLUE),
					new Stop(1.0, Color.WHITE)
			};
			return new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
		}
		
		if (paintname.equals("Green")) {
			Stop[] stops = new Stop[] {
					new Stop(0.0, Color.DARKBLUE),
					new Stop(0.086, Color.DARKBLUE),
					new Stop(0.086, Color.DARKGREEN),
					new Stop(0.914, Color.GREEN),
					new Stop(0.914, Color.DARKBLUE),
					new Stop(1.0, Color.DARKBLUE)
			};
			return new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
		}
		
		else return Color.TRANSPARENT;
	}
	
	public boolean isFixed (String paintname) {
		if (paintname.equals("OceanSunset")) return true;
		else return false;
	}
}
