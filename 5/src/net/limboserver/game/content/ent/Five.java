package net.limboserver.game.content.ent;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import net.limboserver.game.engine.helper.ImageFactory;
import net.limboserver.game.engine.simulation.Corporeal;
import net.limboserver.game.engine.simulation.Player;

public class Five extends Corporeal implements Player {

	private final Image sheet = ImageFactory.cache(getSpriteLocation());
	private final Map<String, Image> spritecache;
	private boolean movingHorizontal;
	private boolean movingVertical;
	private boolean playing;
	private boolean twoTwenty;
	private int counter = 0;

	public Five() {
		spritecache = new HashMap<>();

		spritecache.put("up0",
				ImageFactory.snap(sheet, new Rectangle2D(6, 204, 24, 54)));
		spritecache.put("up1",
				ImageFactory.snap(sheet, new Rectangle2D(42, 204, 24, 54)));
		spritecache.put("up2", spritecache.get("up0"));
		spritecache.put("up3",
				ImageFactory.snap(sheet, new Rectangle2D(83, 204, 24, 54)));
		spritecache.put("up4",
				ImageFactory.snap(sheet, new Rectangle2D(83, 204, 24, 54)));

		spritecache.put("down0",
				ImageFactory.snap(sheet, new Rectangle2D(7, 12, 29, 54)));
		spritecache.put("down1",
				ImageFactory.snap(sheet, new Rectangle2D(51, 18, 29, 54)));
		spritecache.put("down2", spritecache.get("down0"));
		spritecache.put("down3",
				ImageFactory.snap(sheet, new Rectangle2D(88, 18, 29, 54)));
		spritecache.put("down4",
				ImageFactory.snap(sheet, new Rectangle2D(88, 18, 29, 54)));

		spritecache.put("left0",
				ImageFactory.snap(sheet, new Rectangle2D(12, 78, 27, 54)));
		spritecache.put("left1",
				ImageFactory.snap(sheet, new Rectangle2D(50, 77, 27, 54)));
		spritecache.put("left2", spritecache.get("left0"));
		spritecache.put("left3",
				ImageFactory.snap(sheet, new Rectangle2D(86, 77, 27, 54)));
		spritecache.put("left4",
				ImageFactory.snap(sheet, new Rectangle2D(86, 77, 27, 54)));

		spritecache.put("right0",
				ImageFactory.snap(sheet, new Rectangle2D(5, 139, 21, 54)));
		spritecache.put("right1",
				ImageFactory.snap(sheet, new Rectangle2D(37, 139, 21, 54)));
		spritecache.put("right2", spritecache.get("right0"));
		spritecache.put("right3",
				ImageFactory.snap(sheet, new Rectangle2D(72, 139, 21, 54)));
		spritecache.put("right4",
				ImageFactory.snap(sheet, new Rectangle2D(72, 139, 21, 54)));
	}

	@Override
	public void signalUp() {
		movingVertical = goUp(getMovementStep());
	}

	@Override
	public void releaseUp() {
		movingVertical = false;
	}

	@Override
	public void signalDown() {
		movingVertical = goDown(getMovementStep());
	}

	@Override
	public void releaseDown() {
		movingVertical = false;
	}

	@Override
	public void signalLeft() {
		movingHorizontal = goLeft(getMovementStep());
	}

	@Override
	public void releaseLeft() {
		movingHorizontal = false;
	}

	@Override
	public void signalRight() {
		movingHorizontal = goRight(getMovementStep());
	}

	@Override
	public void releaseRight() {
		movingHorizontal = false;
	}

	@Override
	public void signalPrimary() {
		playing = true;
	}

	@Override
	public void releasePrimary() {
		playing = false;
	}

	@Override
	public void signalSecondary() {
		twoTwenty = true;
	}

	@Override
	public void releaseSecondary() {
		twoTwenty = false;
	}

	@Override
	protected int getMovementStep() {
		if (twoTwenty)
			return 3;
		else
			return 2;
	}

	@Override
	public Image getCurrentSprite() {
		int frame = 0;
		if (movingHorizontal || movingVertical || twoTwenty) {
			counter++;
			if (counter >= 4 * 30)
				counter = 0;
			frame = counter / 30;
		} else if (playing) {
			counter++;
			if (counter >= 4 * 30)
				counter = 0;
			if (counter < 60)
				frame = 0;
			else
				frame = 4;
		}
		return spritecache.get(getDirectionFacing() + frame);
	}

	@Override
	public Point2D getRenderPoint() {
		double minx = getAnchorPoint().getX()
				- (getCurrentSprite().getWidth() / 2), miny = getAnchorPoint()
				.getY()
				- getCurrentSprite().getHeight()
				+ ImageFactory.scale(2);
		return new Point2D(minx, miny);
	}

	@Override
	public BoundingBox getCollisionBox() {
		final int boundingwidth = ImageFactory.scale(14), boundingheight = ImageFactory
				.scale(30);
		return new BoundingBox(getAnchorPoint().getX() - (boundingwidth / 2)
				- 1, getAnchorPoint().getY() - boundingheight - 1,
				boundingwidth, boundingheight);
	}

	@Override
	public void doPreLogic() {
		movingHorizontal = false;
		movingVertical = false;
	}

	@Override
	public void doLogic() {
		if (this != getWorld().getPlayerEntity()) {
			double playerX = ((Corporeal) getWorld().getPlayerEntity())
					.getAnchorPoint().getX(), myX = getAnchorPoint().getX();
			double playerY = ((Corporeal) getWorld().getPlayerEntity())
					.getAnchorPoint().getY(), myY = getAnchorPoint().getY();

			int distanceX = (int) (Math.abs(playerX - myX) * .005);
			int distanceY = (int) (Math.abs(playerY - myY) * .005);

			movingHorizontal = (distanceX != 0);
			movingVertical = (distanceY != 0);

			if (playerY >= myY)
				movingVertical = goDown((int) distanceY);
			else
				movingVertical = goUp((int) distanceY);
			if (playerX >= myX)
				movingHorizontal = goRight((int) distanceX);
			else
				movingHorizontal = goLeft((int) distanceX);

			if (getWorld().getPlayerEntity().isPlaying())
				playing = true;
			else
				playing = false;
		}
	}

	@Override
	public String getSpriteLocation() {
		return "/net/limboserver/game/content/img/five.png";
	}

	@Override
	public boolean isPlaying() {
		return playing;
	}

	@Override
	public boolean isReusable() {
		return true;
	}

}
