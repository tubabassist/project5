// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.control;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import net.limboserver.game.engine.helper.UserInput;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;

public class Engine extends Application {
	// == MAY BE EDITED == //
	public static final boolean
		DEBUG_ON = false,
		CRISP_PIXELS = true,
		PRESERVE_ASPECT = true;
	
	public static final int
		TARGET_FPS = 60;
	// =================== //
	
	public static final int
		SCREEN_WIDTH = (int) Screen.getPrimary().getBounds().getWidth(),
		SCREEN_HEIGHT = (int) Screen.getPrimary().getBounds().getHeight(),
		RENDER_WIDTH = 16 * 32,
		RENDER_HEIGHT = 16 * 18;
	
	public static final double
		SCALE_FACTOR = (!CRISP_PIXELS ?
			(double) SCREEN_HEIGHT / (double) RENDER_HEIGHT :
			(int) ((double) SCREEN_HEIGHT / (double) RENDER_HEIGHT) 	
		);
	
	public static final int	
		SCALED_VIEW_HEIGHT = (PRESERVE_ASPECT ?
			(int) (RENDER_HEIGHT * SCALE_FACTOR) :
			SCREEN_HEIGHT
		),
		SCALED_VIEW_WIDTH = (PRESERVE_ASPECT ?
			(int) (RENDER_WIDTH * SCALE_FACTOR) :
			SCREEN_WIDTH
		);
	
	@Override
	public void init()  {
		if (Engine.DEBUG_ON)
			System.out.println("Scale Factor: " + SCALE_FACTOR);
	}

	private StackPane gameview;
	
	@Override
	public void start(final Stage stage) throws Exception {
		String bootworld = this.getParameters().getUnnamed().get(0);
		
		stage.setTitle("Game Programming Demo");
		stage.setFullScreen(true);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setFullScreenExitHint("");
		stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		
		final StackPane composite = new StackPane();
		composite.setPrefSize(SCALED_VIEW_WIDTH, SCALED_VIEW_HEIGHT);
		final Scene scene = new Scene(composite, Color.BLACK);
		stage.setScene(scene);
		stage.show();
        
		gameview = new StackPane();
		gameview.setPrefSize(SCALED_VIEW_WIDTH, SCALED_VIEW_HEIGHT);
		composite.getChildren().add(gameview);
		
		if (Engine.DEBUG_ON) { 
			final HBox statusbar = new HBox(25);
			statusbar.setPadding(new Insets(0, 10, 5, 10));
			
			final Text status = new Text("Loading...");
			status.setStyle(
				"-fx-font: xx-large Consolas; -fx-fill: yellow;" +
				"-fx-effect: dropshadow(gaussian, goldenrod, 24, 0.7, 0, 0);"
			);
		    statusbar.getChildren().add(status);
		    
		    final Text fps = new Text("FPS: " + TARGET_FPS);
			fps.setStyle(
					"-fx-font: xx-large Consolas; -fx-fill: springgreen;" +
					"-fx-effect: dropshadow(gaussian, seagreen, 24, 0.7, 0, 0);"
			);
			statusbar.getChildren().add(fps);
			
			final BorderPane overlay = new BorderPane();
			overlay.setPrefSize(SCALED_VIEW_WIDTH, SCALED_VIEW_HEIGHT);
			overlay.setBottom(statusbar);
			composite.getChildren().add(overlay);
			
	    	final Timeline debugloop = new Timeline();
	    	debugloop.setCycleCount(Animation.INDEFINITE);
	    	debugloop.getKeyFrames().add( 
	    		new KeyFrame(
	    			Duration.seconds(1),
	    			new DebugLoop(status, fps)
	    		)
	    	);
	    	debugloop.play();
		}
        
        final Timeline gameloop = new Timeline();
    	gameloop.setCycleCount(Animation.INDEFINITE);
    	gameloop.getKeyFrames().add( 
    		new KeyFrame(
    			Duration.millis(1000.0 / TARGET_FPS),
    			new GameLoop(gameview, bootworld)
    		)
    	);
        gameloop.play();
        
    	EventHandler<KeyEvent> ui = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.PRINTSCREEN))
					Engine.this.printScreen();
				UserInput.process(event);
			}
    	};
    	stage.getScene().setOnKeyPressed(ui);
    	stage.getScene().setOnKeyReleased(ui);
	}
	
	@Override
	public void stop() {
		
	}
	
	public void printScreen() {
        WritableImage output = new WritableImage(SCALED_VIEW_WIDTH, SCALED_VIEW_HEIGHT);
		SnapshotParameters params = new SnapshotParameters();
		params.setFill(Color.TRANSPARENT);
		params.setViewport(new Rectangle2D(
			(SCREEN_WIDTH-SCALED_VIEW_WIDTH)/2,
			(SCREEN_HEIGHT-SCALED_VIEW_HEIGHT)/2,
			SCALED_VIEW_WIDTH,
			SCALED_VIEW_HEIGHT
		));
    	gameview.snapshot(params, output);
    	
    	File file = new File("screenshot-" + UUID.randomUUID() + ".png");
    	try {
			ImageIO.write(SwingFXUtils.fromFXImage(output, null), "png", file);
		}
        catch (IOException e) {
			e.printStackTrace();
		}
	}
}
