// Your Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.control;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;

public class DebugLoop implements EventHandler<ActionEvent> {
	private final Text statusbar, fpsbar;
	private int tick;
	public DebugLoop(Text statusbar, Text fpsbar) {
		this.statusbar = statusbar;
		this.fpsbar = fpsbar;
		this.tick = 0;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		tick++;
		statusbar.setText("Tick " + tick); // + "/Time " + LocalTime.now().get(ChronoField.SECOND_OF_MINUTE));
		fpsbar.setText(fpsbar.getText());
	}
}
