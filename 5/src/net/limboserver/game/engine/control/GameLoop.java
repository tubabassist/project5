// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.control;

import net.limboserver.game.engine.helper.ImageFactory;
import net.limboserver.game.engine.helper.UserInput;
import net.limboserver.game.engine.simulation.Corporeal;
import net.limboserver.game.engine.simulation.Entity;
import net.limboserver.game.engine.simulation.World;

import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.Set;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class GameLoop implements EventHandler<ActionEvent> {
	private final Canvas background, mainplane, foreground;
	private final World map;

	// private boolean changingPlayer = false;
	// private boolean musicon;

	public GameLoop(StackPane viewstack, String worldname) {
		background = new Canvas(Engine.SCALED_VIEW_WIDTH,
				Engine.SCALED_VIEW_HEIGHT);
		viewstack.getChildren().add(background);

		mainplane = new Canvas(Engine.SCALED_VIEW_WIDTH,
				Engine.SCALED_VIEW_HEIGHT);
		viewstack.getChildren().add(mainplane);

		foreground = new Canvas(Engine.SCALED_VIEW_WIDTH,
				Engine.SCALED_VIEW_HEIGHT);
		viewstack.getChildren().add(foreground);

		map = new World(worldname);
		/*
		 * if (map.hasMusic()) { musicon = true; //map.getMusicLooper().start();
		 * } else { musicon = false; }
		 */}

	@Override
	public void handle(ActionEvent arg0) {
		preUpdateLogic();
		processInputs();
		updateLogic();
		renderLayers();
	}

	public void processInputs() {
		if (UserInput.keyDirectionUp())
			map.getPlayerEntity().signalUp();
		if (UserInput.keyDirectionDown())
			map.getPlayerEntity().signalDown();
		if (UserInput.keyDirectionLeft())
			map.getPlayerEntity().signalLeft();
		if (UserInput.keyDirectionRight())
			map.getPlayerEntity().signalRight();

		if (UserInput.keyPrimary()) {
			map.getMusicLooper().start();
			map.getPlayerEntity().signalPrimary();
		}
		if (UserInput.relPrimary()) {
			map.getMusicLooper().stop();
			map.getPlayerEntity().releasePrimary();
		}
		if (UserInput.keySecondary())
			map.getPlayerEntity().signalSecondary();
		if (UserInput.relSecondary())
			map.getPlayerEntity().releaseSecondary();
		
		/*
		 * if (UserInput.keyTertiary()) map.getPlayerEntity().signalTertiary();
		 * if (UserInput.relTertiary()) map.getPlayerEntity().releaseTertiary();
		 * if (UserInput.keyQuaternary() && !changingPlayer) { changingPlayer =
		 * true; map.nextPlayer(); } if (UserInput.relQuaternary()) {
		 * changingPlayer = false; }
		 * 
		 * if (UserInput.keySpecialUp() && !musicon) { musicon = true;
		 * map.getMusicLooper().start(); } else if (UserInput.keySpecialDown()
		 * && musicon) { musicon = false; map.getMusicLooper().pause(); }
		 */}

	public void preUpdateLogic() {
		Set<Entity> removals = new LinkedHashSet<>();
		for (Entity ent : map.getEntities()) {
			ent.doPreLogic();
			if (ent.isMarkedForRemoval()) {
				if (ent == map.getPlayerEntity())
					map.previousPlayer();
				removals.add(ent);
				if (!ent.isReusable())
					ImageFactory.recycle(ent.getSpriteLocation());
			}
		}
		map.getEntities().removeAll(removals);
	}

	public void updateLogic() {
		try {
			for (Entity ent : map.getEntities())
				ent.doLogic();
		} catch (ConcurrentModificationException e) {
		}
	}

	double scrollx = 0.5, scrolly = 0.5;

	public void renderLayers() {

		final int tilesize = map.getTileSize();
		{
			double playerxp = map.getFocusEntity().getAnchorPoint().getX()
					/ map.getRenderWidth(), playeryp = map.getFocusEntity()
					.getAnchorPoint().getY()
					/ map.getRenderHeight();
			double a = 0.65;
			double targetx, targety;
			if (playerxp <= 0.5)
				targetx = Math.pow(2 * playerxp, 1 / a) / 2;
			else
				targetx = 1 - Math.pow(2 * (1 - playerxp), 1 / a) / 2;
			if (playeryp <= 0.5)
				targety = Math.pow(2 * playeryp, 1 / a) / 2;
			else
				targety = 1 - Math.pow(2 * (1 - playeryp), 1 / a) / 2;
			scrollx = scrollx * .97 + targetx * .03;
			scrolly = scrolly * .97 + targety * .03;
		}

		for (int pn = 1; pn <= 3; pn++) {
			if (pn != 2) {
				Canvas plane = (pn == 1 ? background : foreground);

				final GraphicsContext gc = plane.getGraphicsContext2D();
				// gc.clearRect(0, 0, plane.getWidth(), plane.getHeight());

				int offsetx = (int) ((map.getRenderWidth() - plane.getWidth()) * scrollx), offsety = (int) ((map
						.getRenderHeight() - plane.getHeight()) * scrolly);

				final int minrow = Math.max(offsety / tilesize, 0), mincol = Math
						.max(offsetx / tilesize, 0), maxrow = Math.min(
						((int) plane.getHeight() + offsety - 1) / tilesize,
						map.getMapHeight() - 1), maxcol = Math.min(
						((int) plane.getWidth() + offsetx - 1) / tilesize,
						map.getMapWidth() - 1);

				int minlayer, maxlayer;
				if (pn == 1) {
					minlayer = 0;
					maxlayer = map.getBackroundLayers();

					gc.setFill(map.getUnderlayerPaint());
					if (map.isUnderlayerFixed()) {
						gc.fillRect(0, 0, plane.getWidth(), plane.getHeight());
					} else {
						gc.fillRect(0 - offsetx, 0 - offsety,
								map.getRenderWidth(), map.getRenderHeight());
					}
				} else {
					minlayer = map.getBackroundLayers();
					maxlayer = map.getTotalLayers();
				}

				for (int layer = minlayer; layer < maxlayer; layer++) {
					for (int row = minrow; row <= maxrow; row++) {
						for (int col = mincol; col <= maxcol; col++) {
							Image sprite = map.imageAt(row, col, layer);
							if (sprite != null) {
								int x = col * tilesize - offsetx, y = row
										* tilesize - offsety;
								gc.drawImage(sprite, x, y);
							}
						}
					}
				}
			} else {
				final GraphicsContext gc = mainplane.getGraphicsContext2D();
				gc.clearRect(0, 0, mainplane.getWidth(), mainplane.getHeight());

				int offsetx = (int) ((map.getMapWidth() * tilesize - mainplane
						.getWidth()) * scrollx), offsety = (int) ((map
						.getMapHeight() * tilesize - mainplane.getHeight()) * scrolly);

				for (Entity entity : map.getEntities()) {
					if (entity instanceof Corporeal) {
						Corporeal ent = (Corporeal) entity;
						gc.drawImage(ent.getCurrentSprite(), ent
								.getRenderPoint().getX() - offsetx, ent
								.getRenderPoint().getY() - offsety);
						if (Engine.DEBUG_ON) {
							if (ent.getCollisionBox() != null) {
								gc.setStroke(Color.rgb(255, 255, 0, 0.5));
								gc.setLineWidth(ImageFactory.scale(2));
								gc.strokeRect(ent.getCollisionBox().getMinX()
										- offsetx, ent.getCollisionBox()
										.getMinY() - offsety, ent
										.getCollisionBox().getWidth(), ent
										.getCollisionBox().getHeight());
							}
						}
					}
				}
			}
		}
	}
}
