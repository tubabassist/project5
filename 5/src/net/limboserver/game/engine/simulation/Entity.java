// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.simulation;

public abstract class Entity {
	public abstract void doPreLogic();
	public abstract void doLogic();
	
	private World enclosure;
	public void setWorld(World world) { enclosure = world; }
	public World getWorld() { return enclosure; }
	
	private boolean removal = false;
	public void markForRemoval() {
		removal = true;
	}
	public boolean isMarkedForRemoval() {
		return removal;
	}
	
	public abstract String getSpriteLocation();
	public abstract boolean isReusable();
}
