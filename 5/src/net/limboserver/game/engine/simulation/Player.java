// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.simulation;

public interface Player {
	public void signalUp();
	public void releaseUp();
	
	public void signalDown();
	public void releaseDown();
	
	public void signalLeft();
	public void releaseLeft();
	
	public void signalRight();
	public void releaseRight();
	
	public void signalPrimary();
	public void releasePrimary();
	
	public void signalSecondary();
	public void releaseSecondary();
	
	public boolean isPlaying();
}