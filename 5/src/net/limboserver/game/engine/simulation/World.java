// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.simulation;

import net.limboserver.game.engine.control.Engine;
import net.limboserver.game.engine.helper.ImageFactory;
import net.limboserver.game.engine.helper.MusicHandler;
import net.limboserver.game.engine.helper.PaintSheme;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class World {
	private class Tile {
		public char[] symbol;
		public boolean alpha;
	}
	private final Tile[][] map;
	
	private final int mapheight, mapwidth;
	private final int tilesize, tilepadding;
	private final int bglayers, fglayers;
	
	private final Map<Character,Image> tilecache;
	private final Paint underlayer;
	private final boolean underfixed;
	
	private final List<Entity> entities;
	private Entity player;
	private int playerIndex;
	private int previousIndex;
	private Corporeal focus;
	
	private final MusicHandler bgm;
	
	public World(String resourceName) {
		final Scanner res = new Scanner(getClass().getResourceAsStream(resourceName));
		
		res.useDelimiter(Pattern.compile("(?m)(//.+$|\\p{javaWhitespace})+"));
		
		tilesize = res.nextInt();
		tilepadding = res.nextInt();
		
		String colorres = res.next();
		String colorname = res.next();
		PaintSheme scheme = null;
		try {
			scheme = (PaintSheme) Class.forName(colorres).newInstance();
		}
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (scheme != null) {
			underlayer = scheme.getPaint(colorname);
			underfixed = scheme.isFixed(colorname);
		}
		else {
			underlayer = Color.TRANSPARENT;
			underfixed = true;
		}
		
 		final int trackCount = res.nextInt(); 
  		bgm = new MusicHandler();
		for (int i = 0; i < trackCount; i++) {
			String audiofile = res.next();
			Media track = null;
			try {
				track = new Media(getClass().getResource(audiofile).toURI().toString());
			}
			catch (NullPointerException e) {
				track = null;
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			if (track != null) {
				bgm.add(track);
			}
		}
		
		tilecache = new HashMap<>();
		final int aliascount = res.nextInt();
		String sheetname = "";
		for (int i = 0; i < aliascount; i++) {
			final char symbol = res.next().charAt(0);
			if (i == 0 || (res.hasNext() && !res.hasNextInt()))
				sheetname = res.next();
			final int tilerow = res.nextInt();
			final int tilecol = res.nextInt();
			
			final Image sheet = ImageFactory.cache(sheetname);
			final Rectangle2D rect = new Rectangle2D(
				tilepadding + (tilecol-1)*(tilesize+tilepadding),
				tilepadding + (tilerow-1)*(tilesize+tilepadding),
				tilesize,
				tilesize
			);
			tilecache.put(symbol, ImageFactory.snap(sheet, rect));
		}
		
		mapheight = res.nextInt();
		mapwidth = res.nextInt();
		
		bglayers = res.nextInt();
		fglayers = res.nextInt();
		final int layers = bglayers+fglayers;
		
		map = new Tile[mapheight][mapwidth];
		
		res.useDelimiter(Pattern.compile("(?m)(//.+$|\\p{javaWhitespace})+|"));

		for (int layer = 0; layer < layers; layer++) {
			for (int row = 0; row < mapheight; row++) {
				for (int col = 0; col < mapwidth; col++) {
					final char sym = res.next().charAt(0);
					if (layer == 0) {
						map[row][col] = new Tile();
						map[row][col].symbol = new char[layers];
					}
					map[row][col].symbol[layer] = sym;
				}
			}
		}
		
		for (int row = 0; row < mapheight; row++) {
			for (int col = 0; col < mapwidth; col++) {
				final char sym = res.next().charAt(0);
				map[row][col].alpha = (sym == '1');
			}
		}
		
		res.useDelimiter(Pattern.compile("(?m)(//.+$|\\p{javaWhitespace})+"));
		
		int entitycount = res.nextInt();
		entities = new ArrayList<Entity>();
		
		for (int i = 1; i <= entitycount; i++) {
			String entname = res.next();
			Entity entity;

			try {
				entity = (Entity) Class.forName(entname).getConstructor().newInstance();
				entity.setWorld(World.this);
				if (entity instanceof Corporeal) {
					int enty = res.nextInt();
					int entx = res.nextInt();
					((Corporeal)entity).anchorToTile(enty, entx);
				}
				
				entities.add(entity);
				
				if (i == 1) {
					player = entity;
					playerIndex = 0;
					focus = (Corporeal) entity;
				}
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		res.close();
	}
	
	public int getMapHeight() { return mapheight; }
	public int getMapWidth() { return mapwidth; }
	public int getRenderHeight() { return mapheight * getTileSize(); }
	public int getRenderWidth() { return mapwidth * getTileSize(); }
	public int getTileSize() { return (int) (tilesize * Engine.SCALE_FACTOR); }
	public int getBackroundLayers() { return bglayers; }
	public int getForegroundLayers() { return fglayers; }
	public int getTotalLayers() { return bglayers+fglayers; }
	
	public char symbolAt(int row, int col, int layer) { 
		return map[row][col].symbol[layer]; 
	}
	
	public boolean alphaAt(int row, int col) {
		if ( row < 0 || row >= getMapHeight() || col < 0 || col >= getMapWidth()) 
			return true;
		else
			return map[row][col].alpha; 
	}
	public boolean alphaAtPoint(int y, int x) {
		int tilex = (int) Math.round(x / getTileSize()),
			tiley = (int) Math.round(y / getTileSize());
		return alphaAt(tiley, tilex);
	}
	public boolean alphaAtPoint(double y, double x) {
		return alphaAtPoint((int) y, (int) x);
	}
	
	public Image imageAt(int row, int col, int layer) {
		return tilecache.get(symbolAt(row,col,layer));
	}
	
	public Corporeal getFocusEntity() {
		return focus;
	}
	
	public void setFocusEntity(Corporeal entity) {
		focus = entity;
	}
	
	public Player getPlayerEntity() {
		return (Player) player;
	}
	
	public void setPlayerEntity(int index) {
		previousIndex = playerIndex;
		playerIndex = index;
		
		if (entities.get(playerIndex) instanceof Player) {
			player = entities.get(playerIndex);
		}
		else nextPlayer();
	}
	
	public int getPlayerIndex() {
		return playerIndex;
	}
	
	public List<Entity> getEntities() {
		return entities;
	}
	
	public Paint getUnderlayerPaint() {
		return underlayer;
	}
	
	public boolean isUnderlayerFixed() {
		return underfixed;
	}
	
	public MusicHandler getMusicLooper() {
		return bgm;
	}
	
	public boolean hasMusic() {
		return bgm != null;
	}
	
	public void nextPlayer() {
		try {
			setPlayerEntity(playerIndex + 1);
		}
		catch (IndexOutOfBoundsException e) {
			setPlayerEntity(0);
		}
		setFocusEntity((Corporeal) getPlayerEntity());
	}
	
	public void previousPlayer() {
		setPlayerEntity(previousIndex);
		setFocusEntity((Corporeal) getPlayerEntity());
	}
}
