// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.simulation;

import net.limboserver.game.engine.helper.ImageFactory;
import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public abstract class Corporeal extends Entity {
	private int anchorx, anchory;
	private String directionFacing = "left";
	
	public void anchorToTile(int row, int col) {
		anchorx = (int) ((col-0.5) * getWorld().getTileSize());
		anchory = (int) ((row) * getWorld().getTileSize());
	}
	public void anchorToPoint(int y, int x) {
		anchorx = x;
		anchory = y;
	}
	public Point2D getAnchorPoint() {
		return new Point2D(anchorx, anchory);
	}
	
	protected abstract int getMovementStep();
	
	public boolean goUp(int distance) {
		distance = ImageFactory.scale(distance);
		BoundingBox b = getCollisionBox();
		for (int step = distance; step >= 1; step--) {
			if (
				b==null ||
				(!getWorld().alphaAtPoint(b.getMinY()-step, b.getMinX()) &&
				!getWorld().alphaAtPoint(b.getMinY()-step, b.getMaxX()))
			) {
				anchory -= step;
				if (anchory < 0)
					anchory = 0;
				directionFacing = "up";
				return true;
			}
		}
		return false;
	}

	public boolean goDown(int distance) {
		distance = ImageFactory.scale(distance);
		BoundingBox b = getCollisionBox();
		for (int step = distance; step >= 1; step--) {
			if (
				b==null ||
				(!getWorld().alphaAtPoint(b.getMaxY()+step, b.getMinX()) &&
				!getWorld().alphaAtPoint(b.getMaxY()+step, b.getMaxX()))
			) {
				anchory += step;
				if (anchory > getWorld().getRenderHeight())
					anchory = getWorld().getRenderHeight();
				directionFacing = "down";
				return true;
			}
		}
		return false;
	}
	
	public boolean goLeft(int distance) {
		distance = ImageFactory.scale(distance);
		BoundingBox b = getCollisionBox();
		for (int step = distance; step >= 1; step--) {
			if (
				b==null ||
				(!getWorld().alphaAtPoint(b.getMinY(), b.getMinX()-step) &&
				//!getWorld().alphaAtPoint((b.getMinY()+b.getMaxY())/2, b.getMinX()-step) &&
				!getWorld().alphaAtPoint(b.getMaxY(), b.getMinX()-step))
			) {
				anchorx -= step;
				if (anchorx < 0)
					anchorx = 0;
				directionFacing = "left";
				return true;
			}
		}
		return false;
	}

	public boolean goRight(int distance) {
		distance = ImageFactory.scale(distance);
		BoundingBox b = getCollisionBox();
		for (int step = distance; step >= 1; step--) {
			if (
				b==null ||
				(!getWorld().alphaAtPoint(b.getMinY(), b.getMaxX()+step) &&
				//!enclosure.alphaAtPoint((b.getMinY()+b.getMaxY())/2, b.getMaxX()+step) &&
				!getWorld().alphaAtPoint(b.getMaxY(), b.getMaxX()+step))
			) {
				anchorx += step;
				if (anchorx > getWorld().getRenderWidth())
					anchorx = getWorld().getRenderWidth();
				directionFacing = "right";
				return true;
			}
		}
		return false;
	}
	
	public String getDirectionFacing() {
		return directionFacing;
	}
	
	public abstract Image getCurrentSprite();
	public abstract Point2D getRenderPoint();
	public abstract BoundingBox getCollisionBox();
}
