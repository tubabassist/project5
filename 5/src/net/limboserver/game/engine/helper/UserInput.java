// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.helper;

import javafx.application.Platform;
import javafx.event.EventType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class UserInput {
	private static boolean
		ACCEPT, REJECT, KEY_PRIMARY, KEY_SECONDARY, KEY_TERTIARY, KEY_QUATERNARY,
		DIRECTION_UP, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT,
		SPECIAL_UP, SPECIAL_DOWN, MENU;
	
	private static boolean
		REL_PRIMARY, REL_SECONDARY, REL_TERTIARY, REL_QUATERNARY,
		REL_DIRECTION_UP, REL_DIRECTION_DOWN, REL_DIRECTION_LEFT, REL_DIRECTION_RIGHT;
	
	public static boolean keyAccept() { return ACCEPT; }
	
	public static boolean keyReject() { return REJECT; }
	public static void clearReject() { REJECT = false; }
	
	public static boolean keyPrimary() { return KEY_PRIMARY; }
	public static boolean relPrimary() { return REL_PRIMARY; }
	
	public static boolean keySecondary() { return KEY_SECONDARY; }
	public static boolean relSecondary() { return REL_SECONDARY; }
	
	public static boolean keyTertiary() { return KEY_TERTIARY; }
	public static boolean relTertiary() { return REL_TERTIARY; }
	
	public static boolean keyQuaternary() { return KEY_QUATERNARY; }
	public static boolean relQuaternary() { return REL_QUATERNARY; }
	
	public static boolean keyDirectionUp() { return DIRECTION_UP; }
	public static boolean relDirectionUp() { return REL_DIRECTION_UP; }
	public static boolean keyDirectionDown() { return DIRECTION_DOWN; }
	public static boolean relDirectionDown() { return REL_DIRECTION_DOWN; }
	public static boolean keyDirectionLeft() { return DIRECTION_LEFT; }
	public static boolean relkeyDirectionLeft() { return REL_DIRECTION_LEFT; }
	public static boolean keyDirectionRight() { return DIRECTION_RIGHT; }
	public static boolean relDirectionRight() { return REL_DIRECTION_RIGHT; }
	
	public static boolean keyMenu() { return MENU; }
	public static boolean keySpecialUp() { return SPECIAL_UP; }
	public static boolean keySpecialDown() { return SPECIAL_DOWN; }
	
	public static void process(KeyEvent event) {
		final KeyCode code = event.getCode();
		final EventType<KeyEvent> type = event.getEventType();
		
		if (!(type.equals(KeyEvent.KEY_PRESSED) || type.equals(KeyEvent.KEY_RELEASED))) {
			return;
		}
		else if (code.equals(KeyCode.SPACE)) { //2
			KEY_PRIMARY = type.equals(KeyEvent.KEY_PRESSED);
			REL_PRIMARY = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.SHIFT)) { //1
			KEY_SECONDARY = type.equals(KeyEvent.KEY_PRESSED);
			REL_SECONDARY = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.CONTROL)) {
			KEY_TERTIARY = type.equals(KeyEvent.KEY_PRESSED);
			REL_TERTIARY = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.TAB)) {
			KEY_QUATERNARY = type.equals(KeyEvent.KEY_PRESSED);
			REL_QUATERNARY = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.ENTER)) { //A
			ACCEPT = type.equals(KeyEvent.KEY_PRESSED);
		}
		else if (code.equals(KeyCode.BACK_SPACE)) { //B
			REJECT = type.equals(KeyEvent.KEY_PRESSED);
		}
		else if (code.equals(KeyCode.UP)) {
			DIRECTION_UP = type.equals(KeyEvent.KEY_PRESSED);
			REL_DIRECTION_UP = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.DOWN)) {
			DIRECTION_DOWN = type.equals(KeyEvent.KEY_PRESSED);
			REL_DIRECTION_DOWN = type.equals(KeyEvent.KEY_RELEASED);
		}		
		else if (code.equals(KeyCode.LEFT)) {
			DIRECTION_LEFT = type.equals(KeyEvent.KEY_PRESSED);
			REL_DIRECTION_LEFT = type.equals(KeyEvent.KEY_RELEASED);
		}		
		else if (code.equals(KeyCode.RIGHT)) {
			DIRECTION_RIGHT = type.equals(KeyEvent.KEY_PRESSED);
			REL_DIRECTION_RIGHT = type.equals(KeyEvent.KEY_RELEASED);
		}
		else if (code.equals(KeyCode.PAGE_UP)) { //+
			SPECIAL_UP = type.equals(KeyEvent.KEY_PRESSED);
		}
		else if (code.equals(KeyCode.PAGE_DOWN)) { //-
			SPECIAL_DOWN = type.equals(KeyEvent.KEY_PRESSED);
		}
		else if (code.equals(KeyCode.ESCAPE)) { //HOME
			Platform.exit();
		}
	}
}
