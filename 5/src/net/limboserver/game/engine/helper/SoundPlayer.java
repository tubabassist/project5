// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.helper;

import javafx.scene.media.AudioClip;

public class SoundPlayer {
	public static void playClip(String clipname) {
		AudioClip clip;
		try {
			clip = new AudioClip(SoundPlayer.class.getResource(clipname).toURI().toString());
			clip.play();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
