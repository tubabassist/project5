// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.helper;

import net.limboserver.game.engine.control.Engine;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Rectangle2D;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ImageFactory {
	private static final Map<String,TrackableImage> imgcache = new HashMap<>();
	
	private static class TrackableImage {
		public Image image = null;
		public int counter = 0;
		
		public TrackableImage(Image image) {
			this.image = image;
			counter = 1;
		}
	}
	
	public static Image cache(String resourceName) {
		if (imgcache.containsKey(resourceName)) {
			imgcache.get(resourceName).counter++;
			return imgcache.get(resourceName).image;
		}
		else {
			Image physimg = new Image(ImageFactory.class.getResourceAsStream(resourceName));
			TrackableImage scaleimg = new TrackableImage(resample(physimg, Engine.SCALE_FACTOR));
			imgcache.put(resourceName, scaleimg);
			return scaleimg.image;
		}
	}
	
	public static void recycle(String resourceName){
		if (imgcache.containsKey(resourceName)) {
			imgcache.get(resourceName).counter--;
			if (imgcache.get(resourceName).counter <= 0) {
				imgcache.remove(resourceName);
			}
		}
	}
	
	public static Image resample(Image input, double scale) {
		final int width = (int) input.getWidth(),
				  height = (int) input.getHeight();

		WritableImage output = new WritableImage(
			(int) (width * scale) +1,
			(int) (height * scale) +1
		);

		PixelReader reader = input.getPixelReader();
		PixelWriter writer = output.getPixelWriter();

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				final int argb = reader.getArgb(x, y);
				for (int dy = 0; dy < scale; dy++) {
					for (int dx = 0; dx < scale; dx++) {
						writer.setArgb(
							(int) (x * scale + dx),
							(int) (y * scale + dy),
							argb
						);
					}
				}
			}
		}

		return output;
	}

	public static Image snap(Image sheet, Rectangle2D rect) {
		final ImageView sprite = new ImageView(sheet);
		Rectangle2D scaled = new Rectangle2D(
			(int) (rect.getMinX() * Engine.SCALE_FACTOR),
			(int) (rect.getMinY() * Engine.SCALE_FACTOR),
			(int) (rect.getWidth() * Engine.SCALE_FACTOR),
			(int) (rect.getHeight() * Engine.SCALE_FACTOR)
		);
		sprite.setViewport(scaled);
		sprite.setCache(true);
		SnapshotParameters params = new SnapshotParameters();
		params.setFill(Color.TRANSPARENT);
		return sprite.snapshot(params, null);
	}
	
	public static Image mirror(Image snap) {
		final ImageView sprite = new ImageView(snap);
		sprite.setCache(true);
		sprite.setScaleX(-1);
		SnapshotParameters params = new SnapshotParameters();
		params.setFill(Color.TRANSPARENT);
		return sprite.snapshot(params, null);
	}
	
	public static int scale(double coordinate) {
		return (int) (coordinate * Engine.SCALE_FACTOR);
	}
}
