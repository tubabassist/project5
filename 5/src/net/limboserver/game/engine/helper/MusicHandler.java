// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.helper;

import java.util.ArrayList;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class MusicHandler {
	private ArrayList<MediaPlayer> tracks;
	private int currentTrack;
	private boolean playing = false;

	public MusicHandler() {
		tracks = new ArrayList<MediaPlayer>();
	}

	public void add(final Media track) {
		tracks.add(new MediaPlayer(track));
	}

	public void start() {
		if (!playing) {
			currentTrack = (int) (Math.random() * tracks.size());
			tracks.get(currentTrack).play();
			playing = true;
		}
	}

	public void stop() {
		if (playing)
			tracks.get(currentTrack).stop();
		playing = false;
	}

	public void restart(int loop) {
		tracks.get(currentTrack).setStartTime(Duration.millis(loop));
	}
}