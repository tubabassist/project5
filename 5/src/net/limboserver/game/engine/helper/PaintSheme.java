// Joshua Davis
// Project 3: Game Dev, Spring 2014
// CS 111, Lab Section 12
// Bonus Attempted: Yes

package net.limboserver.game.engine.helper;

import javafx.scene.paint.Paint;

public interface PaintSheme {
	public Paint getPaint(String paintname);
	public boolean isFixed(String paintname);
}
